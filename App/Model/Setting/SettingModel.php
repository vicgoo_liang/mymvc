<?php
namespace App\Model\Setting;

use App\Model\Model;

class SettingModel extends Model
{

    public function getSettings()
    {
        return $this->getDb()->get("setting");
    }
}