<?php
namespace App\Model\Setting;

use App\System\Component\Spl\SplBean;

class SettingBean extends SplBean
{

    protected $key;

    protected $value;
    /**
     * @return the $key
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return the $value
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param field_type $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @param field_type $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

}