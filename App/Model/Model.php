<?php
namespace App\Model;

use App\System\Db\MysqliDb;
use App\System\Component\Di;

class Model
{

    function getDb(): MysqliDb
    {
        return Di::getInstance()->get('mysql');
    }
}