<?php

namespace App\Middle\Index;

use App\System\Middle\Middle;

class IndexMiddle extends Middle {
	public function meta() {
		$this->setMeta ( '首页' );
		$this->setName ( '首页' );
		$this->setCrumb ( [ 
				[ 
						'name' => '首页',
						'url' => __ROOT__ . '/' 
				] 
		] );
		$this->stop ( "错误", 600, $_SERVER ['HTTP_HOST'] );
		return $this->run ( [ 
				'pageInfo' => $this->pageInfo 
		] );
	}
}