<?php
namespace App\System\Component\View;

abstract class View
{

    public $twig;

    public $config;

    private $data = array();

    /**
     * 给变量赋值
     *
     * @param string|array $var            
     * @param string $value            
     */
    abstract public function assign($var, $value = NULL);

    /**
     * 模版渲染
     *
     * @param string $template
     *            模板名
     * @param array $data
     *            变量数组
     * @param string $return
     *            true返回 false直接输出页面
     * @return string
     */
    abstract public function render($template, $data = array(), $return = FALSE);

    /**
     * 获取模版名
     *
     * @param string $template            
     */
    abstract public function getTemplateName($template);

    /**
     * 字符串渲染
     *
     * @param string $string
     *            需要渲染的字符串
     * @param array $data
     *            变量数组
     * @param string $return
     *            true返回 false直接输出页面
     * @return string
     */
    abstract public function parse($string, $data = array(), $return = FALSE);
}