<?php

namespace App\HttpController\Api;

use App\System\Http\AbstractInterface\Controller;
use App\System\Component\Execl\Reader\SpreadsheetReader;

class Upload extends Controller {
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \App\System\Http\AbstractInterface\Controller::index()
	 */
	public function index() {
		var_dump ( $this->request ()->getUploadedFile ( "execl" )->moveTo ( $_SERVER ['DOCUMENT_ROOT'] . "/Public/aaa.xlsx" ) );
		
		// var_dump ( $this->request ()->getUploadedFiles () );
	}
	public function index2() {
		$this->response()->withHeader('Content-type', 'text/html; charset=gbk;');
		$this->response()->withHeader('X-Powered-By', 'PHP/6.0.0');
		//header('X-Powered-By: PHP/6.0.0'); //修改 X-Powered-By信息
		//$file = $this->request ()->getUploadedFile ( "execl" );
		$Filepath = $_SERVER ['DOCUMENT_ROOT'] . "/Public/aaa.xlsx";
		//$file->moveTo ( $Filepath );
		
		$StartMem = memory_get_usage ();
		echo '---------------------------------<br>' . PHP_EOL;
		echo 'Starting memory: ' . $StartMem . "<br>" . PHP_EOL;
		echo '---------------------------------<br>' . PHP_EOL;
		
		try {
			$Spreadsheet = new SpreadsheetReader ( $Filepath );
			$BaseMem = memory_get_usage ();
			
			$Sheets = $Spreadsheet->Sheets ();
			
			echo '---------------------------------<br>' . PHP_EOL;
			echo 'Spreadsheets:' . PHP_EOL;
			$this->dump ( $Sheets );
			echo '---------------------------------<br>' . PHP_EOL;
			echo '---------------------------------<br>' . PHP_EOL;
			
			foreach ( $Sheets as $Index => $Name ) {
				echo '---------------------------------<br>' . PHP_EOL;
				echo '*** Sheet ' . $Name . ' ***<br>' . PHP_EOL;
				echo '---------------------------------<br>' . PHP_EOL;
				
				$Time = microtime ( true );
				
				$Spreadsheet->ChangeSheet ( $Index );
				
				foreach ( $Spreadsheet as $Key => $Row ) {
					echo $Key . ': ';
					if ($Row) {
						$this->dump ( $Row );
					} else {
						$this->dump ( $Row );
					}
					$CurrentMem = memory_get_usage ();
					
					echo 'Memory: ' . ($CurrentMem - $BaseMem) . ' current, ' . $CurrentMem . ' base' . PHP_EOL;
					echo '---------------------------------<br>' . PHP_EOL;
					
					if ($Key && ($Key % 500 == 0)) {
						echo '---------------------------------<br>' . PHP_EOL;
						echo 'Time: ' . (microtime ( true ) - $Time);
						echo '---------------------------------<br>' . PHP_EOL;
					}
				}
				
				echo PHP_EOL . '---------------------------------' . PHP_EOL;
				echo 'Time: ' . (microtime ( true ) - $Time);
				echo PHP_EOL;
				
				echo '---------------------------------<br>' . PHP_EOL;
				echo '*** End of sheet ' . $Name . ' ***<br>' . PHP_EOL;
				echo '---------------------------------<br>' . PHP_EOL;
			}
		} catch ( Exception $E ) {
			echo $E->getMessage ();
		}
	}
}