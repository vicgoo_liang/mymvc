<?php
namespace App\HttpController\Execl;

use App\System\Http\AbstractInterface\Controller;
use App\System\Component\Execl\Writer\XLSXWriter;
use App\System\Component\Execl\Reader\SpreadsheetReader;
use Exception;

class Index extends Controller
{

    public function index()
    {
        $filename = "example.csv";
        header('Content-disposition: attachment; filename="' . XLSXWriter::sanitize_filename($filename) . '"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        
        $rows = array(
            array(
                '2003',
                '1',
                '-50.5',
                '2010-01-01 23:00:00',
                '2012-12-31 23:00:00'
            ),
            array(
                '2003',
                '=B1',
                '23.5',
                '2010-01-01 00:00:00',
                '2012-12-31 00:00:00'
            )
        );
        
        $writer = new XLSXWriter();
        $writer->setAuthor('Some Author');
        foreach ($rows as $row)
            $writer->writeSheetRow('Sheet1', $row);
        $writer->writeToStdOut();
        // $writer->writeToFile('example.xlsx');
        // echo $writer->writeToString();
        exit(0);
    }

    function csv()
    {
        $Filepath = $_SERVER['DOCUMENT_ROOT'] . "/Public/ClinicalTraits.csv";
        
        $StartMem = memory_get_usage();
        echo '---------------------------------<br>' . PHP_EOL;
        echo 'Starting memory: ' . $StartMem . "<br>" . PHP_EOL;
        echo '---------------------------------<br>' . PHP_EOL;
        
        try {
            $Spreadsheet = new SpreadsheetReader($Filepath);
            $BaseMem = memory_get_usage();
            
            $Sheets = $Spreadsheet->Sheets();
            
            echo '---------------------------------<br>' . PHP_EOL;
            echo 'Spreadsheets:' . PHP_EOL;
            $this->dump($Sheets);
            echo '---------------------------------<br>' . PHP_EOL;
            echo '---------------------------------<br>' . PHP_EOL;
            
            foreach ($Sheets as $Index => $Name) {
                echo '---------------------------------<br>' . PHP_EOL;
                echo '*** Sheet ' . $Name . ' ***<br>' . PHP_EOL;
                echo '---------------------------------<br>' . PHP_EOL;
                
                $Time = microtime(true);
                
                $Spreadsheet->ChangeSheet($Index);
                
                foreach ($Spreadsheet as $Key => $Row) {
                    echo $Key . ': ';
                    if ($Row) {
                        $this->dump($Row);
                    } else {
                        $this->dump($Row);
                    }
                    $CurrentMem = memory_get_usage();
                    
                    echo 'Memory: ' . ($CurrentMem - $BaseMem) . ' current, ' . $CurrentMem . ' base' . PHP_EOL;
                    echo '---------------------------------<br>' . PHP_EOL;
                    
                    if ($Key && ($Key % 500 == 0)) {
                        echo '---------------------------------<br>' . PHP_EOL;
                        echo 'Time: ' . (microtime(true) - $Time);
                        echo '---------------------------------<br>' . PHP_EOL;
                    }
                }
                
                echo PHP_EOL . '---------------------------------' . PHP_EOL;
                echo 'Time: ' . (microtime(true) - $Time);
                echo PHP_EOL;
                
                echo '---------------------------------<br>' . PHP_EOL;
                echo '*** End of sheet ' . $Name . ' ***<br>' . PHP_EOL;
                echo '---------------------------------<br>' . PHP_EOL;
            }
        } catch (Exception $E) {
            echo $E->getMessage();
        }
    }

    function xlsx()
    {
        $Filepath = $_SERVER['DOCUMENT_ROOT'] . "/Public/test.xlsx";
        
        $StartMem = memory_get_usage();
        echo '---------------------------------<br>' . PHP_EOL;
        echo 'Starting memory: ' . $StartMem . "<br>" . PHP_EOL;
        echo '---------------------------------<br>' . PHP_EOL;
        
        try {
            $Spreadsheet = new SpreadsheetReader($Filepath);
            $BaseMem = memory_get_usage();
            
            $Sheets = $Spreadsheet->Sheets();
            
            echo '---------------------------------<br>' . PHP_EOL;
            echo 'Spreadsheets:' . PHP_EOL;
            $this->dump($Sheets);
            echo '---------------------------------<br>' . PHP_EOL;
            echo '---------------------------------<br>' . PHP_EOL;
            
            foreach ($Sheets as $Index => $Name) {
                echo '---------------------------------<br>' . PHP_EOL;
                echo '*** Sheet ' . $Name . ' ***<br>' . PHP_EOL;
                echo '---------------------------------<br>' . PHP_EOL;
                
                $Time = microtime(true);
                
                $Spreadsheet->ChangeSheet($Index);
                
                foreach ($Spreadsheet as $Key => $Row) {
                    echo $Key . ': ';
                    if ($Row) {
                        $this->dump($Row);
                    } else {
                        $this->dump($Row);
                    }
                    $CurrentMem = memory_get_usage();
                    
                    echo 'Memory: ' . ($CurrentMem - $BaseMem) . ' current, ' . $CurrentMem . ' base' . PHP_EOL;
                    echo '---------------------------------<br>' . PHP_EOL;
                    
                    if ($Key && ($Key % 500 == 0)) {
                        echo '---------------------------------<br>' . PHP_EOL;
                        echo 'Time: ' . (microtime(true) - $Time);
                        echo '---------------------------------<br>' . PHP_EOL;
                    }
                }
                
                echo PHP_EOL . '---------------------------------' . PHP_EOL;
                echo 'Time: ' . (microtime(true) - $Time);
                echo PHP_EOL;
                
                echo '---------------------------------<br>' . PHP_EOL;
                echo '*** End of sheet ' . $Name . ' ***<br>' . PHP_EOL;
                echo '---------------------------------<br>' . PHP_EOL;
            }
        } catch (Exception $E) {
            echo $E->getMessage();
        }
    }

    function xls()
    {
        require_once (__EXECL_READER__ . 'excel_reader2.php');
        
        $Filepath = $_SERVER['DOCUMENT_ROOT'] . "/Public/chat_message.xls";
        
        $StartMem = memory_get_usage();
        echo '---------------------------------<br>' . PHP_EOL;
        echo 'Starting memory: ' . $StartMem . "<br>" . PHP_EOL;
        echo '---------------------------------<br>' . PHP_EOL;
        
        try {
            $Spreadsheet = new SpreadsheetReader($Filepath);
            $BaseMem = memory_get_usage();
            
            $Sheets = $Spreadsheet->Sheets();
            
            echo '---------------------------------<br>' . PHP_EOL;
            echo 'Spreadsheets:' . PHP_EOL;
            $this->dump($Sheets);
            echo '---------------------------------<br>' . PHP_EOL;
            echo '---------------------------------<br>' . PHP_EOL;
            
            foreach ($Sheets as $Index => $Name) {
                echo '---------------------------------<br>' . PHP_EOL;
                echo '*** Sheet ' . $Name . ' ***<br>' . PHP_EOL;
                echo '---------------------------------<br>' . PHP_EOL;
                
                $Time = microtime(true);
                
                $Spreadsheet->ChangeSheet($Index);
                
                foreach ($Spreadsheet as $Key => $Row) {
                    echo $Key . ': ';
                    if ($Row) {
                        $this->dump($Row);
                    } else {
                        $this->dump($Row);
                    }
                    $CurrentMem = memory_get_usage();
                    
                    echo 'Memory: ' . ($CurrentMem - $BaseMem) . ' current, ' . $CurrentMem . ' base' . PHP_EOL;
                    echo '---------------------------------<br>' . PHP_EOL;
                    
                    if ($Key && ($Key % 500 == 0)) {
                        echo '---------------------------------<br>' . PHP_EOL;
                        echo 'Time: ' . (microtime(true) - $Time);
                        echo '---------------------------------<br>' . PHP_EOL;
                    }
                }
                
                echo PHP_EOL . '---------------------------------' . PHP_EOL;
                echo 'Time: ' . (microtime(true) - $Time);
                echo PHP_EOL;
                
                echo '---------------------------------<br>' . PHP_EOL;
                echo '*** End of sheet ' . $Name . ' ***<br>' . PHP_EOL;
                echo '---------------------------------<br>' . PHP_EOL;
            }
        } catch (Exception $E) {
            echo $E->getMessage();
        }
    }

    protected function onRequest($action): ?bool
    {
        return true;
    }

    protected function afterAction($actionName): void
    {}
}