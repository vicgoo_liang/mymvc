<?php

/**
 * Created by PhpStorm.
 * User: yf
 * Date: 2018/3/6
 * Time: 上午11:41
 */
namespace App\HttpController;

use FastRoute\RouteCollector;
use App\System\Http\AbstractInterface\Router as RouterInterface;
use App\System\Http\Request;
use App\System\Http\Response;
use App\System\Component\Di;

class Router extends RouterInterface {
	function register(RouteCollector $routeCollector) {
		// TODO: Implement register() method.
		// 路由拦截若未执行end，请求还会继续进入系统自带的控制器路由匹配
		$routeCollector->get ( '/register', '/Api/Common/register' );
		
		// // 这个路由有，[/{name}] 可选择匹配部分
		// $routeCollector->addRoute('GET', '/user/{id:\d+}[/{name}]', 'handler');
		// // 等同于这两个路由
		// $routeCollector->addRoute('GET', '/user/{id:\d+}', 'handler');
		// $routeCollector->addRoute('GET', '/user/{id:\d+}/{name}', 'handler');
		
		// // 多层嵌套可选路由，也是支持的
		// $routeCollector->addRoute('GET', '/user[/{id:\d+}[/{name}]]', 'handler');
		
		// $routeCollector->get ( '/loginIndex', '/Api/Login/index' );
		// // /loginIndex/1.html
		// $routeCollector->get ( '/loginIndex/{id:\d+}', '/Api/Login/index' );
		// // /loginIndex/1/jll.html
		// $routeCollector->get ( '/loginIndex/{id:\d+}/{name:\d+}', '/Api/Login/index' );
		
		// 上面的三个个路由可以合并
		$routeCollector->get ( '/loginIndex[/{id:\d+}[/{name:\d+}]]', '/Api/Login/index' );
		
		$routeCollector->get ( '/checkIsLogin', '/Api/Common/checkIsLogin' );
		$routeCollector->get ( '/getFriendRequests', '/Api/Friend/getFriendRequests' );
		$routeCollector->get ( '/dealFriendRequest', '/Api/Friend/dealFriendRequest' );
		$routeCollector->get ( '/getFriendList', '/Api/Friend/getFriendList' );
		
		// 测试URL /a
		$routeCollector->get ( '/a', function (Request $request, Response $response) {
			
			$response->withHeader ( 'Content-type', 'text/html; charset=utf8;' );
			$response->withHeader ( 'X-Powered-By', 'PHP/7.2.5' );
			
			$twig = Di::getInstance ()->get ( 'twig' );
			$twig->assign ( "aaaa", "11111" );
			$twig->assign ( "navigation", [ 
					"aaaaa",
					"bbbbb",
					"ccccc" 
			] );
			$re = $twig->getTempLate ( "index" );
			
			$response->write ( $re );
			$response->write ( 'this is write by router with not end ' );
			$response->response ();
		} );
		// 测试URL /a2
		$routeCollector->get ( '/a2', function (Request $request, Response $response) {
			$response->write ( 'this is write by router2 with end ' );
			$response->response ();
		} );
		$routeCollector->post ( '/a3', function (Request $request, Response $response) {
			$response->write ( 'this is write by router2 with end ' );
			$response->response ();
		} );
		// /user/1/index.html
		$routeCollector->get ( '/user/{id:\d+}', function (Request $request, Response $response, $id) {
			$response->write ( "this is router user ,your id is {$id}" );
			$response->response ();
		} );
		// 支持三种逻辑/articles/1 -> /articles/1/1 ->/articles/1/1
		$routeCollector->addRoute ( 'GET', '/articles/{id:\d+}[/{title:\d+}[/{info}]]', 'get_article_handler' );
	}
	public function getMethodNotAllowCallBack() {
		/*
		 * //需要处理逻用回调函数
		 * return function () {
		 * echo 111;
		 * };
		 */
		return null;
	}
}