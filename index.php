<?php
define(__ROOT__, __DIR__ . '/');
define(__CONFIG__, __DIR__ . '/Data/Config/');
define(__EXECL_READER__, __ROOT__ . "/App/System/Component/Execl/Reader/");

require_once 'vendor/autoload.php';

use App\System\App;
App::run()->send();
