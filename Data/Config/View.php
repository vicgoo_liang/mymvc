<?php
return [
    // 默认扩展名
    "extension" => ".html",
    // 默认模版路径
    "template_dir" => "./App/View/",
    // 缓存目录
    "cache_dir" => "./Data/Cache/TwigCache/",
    // 是否开启调试模式
    "debug" => false,
    // 自动刷新
    "auto_reload" => true
];

