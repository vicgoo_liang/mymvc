<?php
return [ 
		"serive_name" => "easymvc", // 服务名称
		"debug" => true,
		"default_timezone" => "Asia/Shanghai", // 时区的配置
		"method" => "Pretty", // Pretty,Json,PlainText,Xml,//错误新的返回格式
		"entrance" => "index.php", // 入口文件名称
		"HTTP_CONTROLLER_MAX_DEPTH" => 10, // 控制路由的深度
		"controllerNameSpace" => 'App\\HttpController\\', // 设置根命名空间
		"Allow_Origin" => false 
];